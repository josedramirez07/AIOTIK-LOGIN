<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
session_start();
if (!isset($_SESSION["user"])) {
    header("Location: login.php");
    exit();
}
?>
<html>
    <head>
        <title> <?php
            echo "Perfil de " . $_SESSION["user"];
            ?> </title>
        <link rel="stylesheet" href="styles/style.css">
        <?php include './includes/head.php'; ?>
    </head>

    <body>
        <?php include './includes/header.php'; ?>

        <main>
            <div class="container profile_container">
                <?php
                echo "<h>Bienvenido, " . $_SESSION["user"] . "!</h>";
                echo "<br>";
                if ($_SESSION["last_login"] != NULL) {
                    echo "<h>Tu último inicio de sesión fue el " . $_SESSION["last_login"] . ".</h>";
                }
                ?>
                <form method="post" action="logoutAction.php">
                    <button class="aiotik_button" type="submit">Cerrar Sesión</button>
                </form>
            </div>


        </main>

        <?php include './includes/footer.php'; ?>
    </body>
</html>
