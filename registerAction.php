<?php

require_once "./includes/database.php";
session_start();

function dbError() {
    $_SESSION["database_error"] = TRUE;
    header("Location: register.php");
    exit();
}

$user = filter_input(INPUT_POST, 'user');
$pass = filter_input(INPUT_POST, 'password');
$hash_password = password_hash($pass, PASSWORD_DEFAULT, ['cost' => 12]);

$query_user = "SELECT 1 FROM `aiotik_user_credentials` WHERE user = '$user' LIMIT 1";
$query_register = "INSERT INTO `aiotik_user_credentials`(`user`, `hash_password`, `registration_date`, `last_login`) VALUES ('$user', '$hash_password', NOW(), NULL)";

$con = db_connect();

if (!$con) {
    dbError();
}

$result = $con->query($query_user);

if ($result -> fetch_row()) {
    $_SESSION["username_exists_error"] = TRUE;
    header("Location: register.php");
} else {
    $result = $con->query($query_register);
    
    if (!$result) {
        dbError();
    } else {
        header("Location: registerSuccessful.php");
    }
}