<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php session_start(); ?>
<html>
    <head>
        <title>Aiotik Register</title>
        <link rel="stylesheet" href="styles/style.css">
        <?php include './includes/head.php'; ?>
    </head>


    <body>
        <?php include './includes/header.php'; ?>

        <main>
            <div class="container">
                <h4>¡Te has registrado! Intenta <a href="login.php">iniciar sesión</a>.</h4>
            </div>
        </main>

        <?php include './includes/footer.php'; ?>
    </body>
</html>
