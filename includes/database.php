<?php

const DB_SERVER = "127.0.0.1";
const DB_USER = "root";
const DB_PASS = "";
const DB_NAME = "test";

function db_connect() {
    $connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
    return $connection;
}
