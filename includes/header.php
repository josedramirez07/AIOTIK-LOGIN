<style>
    #header {
        position: relative;
        display: flex;
        height: 65px;
        flex: none;
        color: white;
        background: #2d2d2d;
        box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.12), 0 1px 5px 0 rgba(0, 0, 0, 0.2);
        
        align-items: center;
        justify-content: center;
    }
    
    #header img {
        border-radius: 50%;
        height: 80%;
        width: auto;
        margin-right: 15px;
    }
    
    
</style>

<header id="header">
    <img src="media/aiotik_logo.jpg" alt="logo"/>
    <h2>Bienvenido a AIOTIK!</h2>
</header>

