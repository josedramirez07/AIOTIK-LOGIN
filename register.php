<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php session_start(); ?>
<html>
    <head>
        <title>Aiotik Register</title>
        <link rel="stylesheet" href="styles/style.css">
        <?php include './includes/head.php'; ?>
    </head>


    <body>
        <?php include './includes/header.php'; ?>

        <main>
            <form class="forma container" action="registerAction.php" method="post">
                <?php
                $message = NULL;

                if (isset($_SESSION["username_exists_error"])) {
                    $message = "El usuario que eligió ya existe.";
                    unset($_SESSION["username_exists_error"]);
                } elseif (isset($_SESSION["database_error"])) {
                    $message = "Ha ocurrido un error de conexión. Intente de nuevo.";
                    unset($_SESSION["database_error"]);
                }

                if (isset($message)) {
                    echo "<label class=\"error\"><b>" . $message . "</b></label>";
                }
                ?>

                <label for="user"><b>Usuario</b></label><br>
                <input type="text" placeholder="Usuario" name="user" id="user" autocomplete="off" maxlength="20" required><br>

                <label for="password"><b>Contraseña</b></label><br>
                <input type="password" placeholder="Contraseña" name="password" id="password" minlength="8" maxlength="40" required><br>

                <button class="aiotik_button" type="submit">Registrarse</button>
            </form>
        </main>

        <?php include './includes/footer.php'; ?>
    </body>
</html>