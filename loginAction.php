<?php

require_once "./includes/database.php";
session_start();

function dbError() {
    $_SESSION["database_error"] = TRUE;
    header("Location: login.php");
    exit();
}

$user = filter_input(INPUT_POST, 'user');
$pass = filter_input(INPUT_POST, 'password');
//$hash_password = password_hash(filter_input(INPUT_POST, 'password'), PASSWORD_DEFAULT, ['cost' => 12]);

$query_user = "SELECT * FROM `aiotik_user_credentials` WHERE user = '$user' LIMIT 1";
$query_update_last_login = "UPDATE `aiotik_user_credentials` SET last_login = NOW() WHERE user = '$user'";

$con = db_connect();

if (!$con) {
    dbError();
}

$result = $con->query($query_user);

if ($result && ($userData = $result->fetch_object()) && password_verify($pass, $userData->hash_password)) {
    $_SESSION["user"] = $userData->user;
    $_SESSION["last_login"] = $userData->last_login;

    $result = $con->query($query_update_last_login);

    if (!$result) {
        dbError();
    }

    header("Location: profile.php");
} else {
    $_SESSION["login_error"] = TRUE;
    header("Location: login.php");
}